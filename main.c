#include <stdio.h>

extern int juce_shared (void);

int main(void)
{
    puts("This is a shared library test...");
    juce_shared();
    return 0;
}