# export LD_LIBRARY_PATH=`pwd`/build:$LD_LIBRARY_PATH
SHELL = /bin/bash

APP = test
SOURCES =  main.c
CFLAGS = -Wall -g
CC = g++
INCLUDES =
LIBS = -lsimpleconsole
LDPATH = ./build
LDFLAGS = -L${LDPATH}

${APP}: clean
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ ${SOURCES} ${LIBS}
#	export LD_LIBRARY_PATH=${LDPATH}:${LD_LIBRARY_PATH}

clean:
	rm -f *.o core *.core ${APP}
